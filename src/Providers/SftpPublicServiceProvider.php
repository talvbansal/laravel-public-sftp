<?php
/**
 * Created by PhpStorm.
 * User: talv
 * Date: 28/04/17
 * Time: 11:55.
 */

namespace TalvBansal\SftpPublic\Providers;

use Illuminate\Support\ServiceProvider;
use League\Flysystem\Filesystem;
use Storage;
use TalvBansal\SftpPublic\PublicSftpAdapter;

class SftpPublicServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // Merge the config new into the existing filesystems config...
        $this->mergeConfigFrom(
            PUBLIC_SFTP_BASE_PATH.'/config/public-sftp.php',
            'filesystems.disks'
        );

        // Register new adapter...
        Storage::extend('public-sftp', function ($app, $config) {
            unset($config['driver']);
            foreach ($config as $key => $value) {
                if (!strlen($value)) {
                    unset($config[$key]);
                }
            }
            $adapter = new PublicSftpAdapter($config);
            return new Filesystem($adapter);
        });
    }
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        if (!defined('PUBLIC_SFTP_BASE_PATH')) {
            define('PUBLIC_SFTP_BASE_PATH', realpath(__DIR__.'/../../'));
        }
    }
}
