<?php
/**
 * Created by PhpStorm.
 * User: talv
 * Date: 28/04/17
 * Time: 12:06.
 */

namespace TalvBansal\SftpPublic;

use League\Flysystem\Sftp\SftpAdapter;

class PublicSftpAdapter extends SftpAdapter
{
    /**
     * Add a new 'public' value to the list of configurable options...
     * @var array
     */
    protected $configurable = [
        'public', 'host', 'hostFingerprint', 'port',
        'username', 'password', 'useAgent',
        'agent', 'timeout', 'root',
        'privateKey', 'permPrivate',
        'permPublic', 'directoryPerm',
        'NetSftpConnection'
    ];

    /**
     * @var string
     */
    protected $publicUrl = '';

    /**
     * Get the publicly accessible Url for a file...
     *
     * @param $path
     *
     * @return string
     */
    public function getUrl($path)
    {
        return $this->publicUrl . DIRECTORY_SEPARATOR . ltrim($path, '/');
    }

    /**
     * Set a publicly accessible Url for files to be accessed from...
     * @param $path
     */
    protected function setPublic($path)
    {
        $this->publicUrl = rtrim($path, '/');
    }
}
