#!/bin/bash

# Install dependencies only for Docker.
[[ ! -e /.dockerinit ]] && [[ ! -e  /.dockerenv ]] && exit 0

# Enable modules
docker-php-ext-enable zip

# Install project dependencies.
composer install --no-suggest --prefer-dist > /dev/null

