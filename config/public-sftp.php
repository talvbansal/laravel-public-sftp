 <?php
/**
 * Created by PhpStorm.
 * User: talv
 * Date: 28/04/17
 * Time: 12:12.
 */

return [
    'public-sftp' => [
        'driver' => 'public-sftp',
        'host' => env('SFTP_HOST'),
        'port' => env('SFTP_PORT', 22),
        'username' => env('SFTP_USERNAME'),
        'password' => env('SFTP_PW', null),
        'privateKey' => env('SFTP_PRIVATE_KEY', null),
        'root' => env('SFTP_ROOT'),
        'timeout' => env('SFTP_TIMEOUT', 10),
        'public' => env('SFTP_PUBLIC_ROOT'),
    ],
];