<?php
/**
 * Created by PhpStorm.
 * User: talv
 * Date: 01/05/17
 * Time: 22:01.
 */

namespace Tests;


class ConfigIsMergedTest extends TestCase
{

    public function test_is_config_merged()
    {
        $disks = config('filesystems.disks');
        $this->assertArrayHasKey('public-sftp', $disks);
        $this->assertArrayHasKey('driver', $disks['public-sftp']);
        $this->assertArrayHasKey('host', $disks['public-sftp']);
        $this->assertArrayHasKey('port', $disks['public-sftp']);
        $this->assertArrayHasKey('username', $disks['public-sftp']);
        $this->assertArrayHasKey('password', $disks['public-sftp']);
        $this->assertArrayHasKey('privateKey', $disks['public-sftp']);
        $this->assertArrayHasKey('root', $disks['public-sftp']);
        $this->assertArrayHasKey('timeout', $disks['public-sftp']);
        $this->assertArrayHasKey('public', $disks['public-sftp']);
    }

}