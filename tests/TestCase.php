<?php
/**
 * Created by PhpStorm.
 * User: talv
 * Date: 01/05/17
 * Time: 22:01.
 */

namespace Tests;


use TalvBansal\SftpPublic\Providers\SftpPublicServiceProvider;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';


    /**
     * @param \Illuminate\Foundation\Application $app
     *
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [SftpPublicServiceProvider::class];
    }
}