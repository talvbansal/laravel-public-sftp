# Public Sftp Adapter for Laravel

> An extension to the [sftp flysystem adapter](http://flysystem.thephpleague.com/adapter/sftp/) that allows you to configure a public URL for files to be accessible from. 

[![build status](https://gitlab.com/talvbansal/laravel-public-sftp/badges/master/build.svg)](https://gitlab.com/talvbansal/laravel-public-sftp/commits/master)
[![Total Downloads](https://poser.pugx.org/talvbansal/laravel-public-sftp/downloads)](https://packagist.org/packages/talvbansal/laravel-public-sftp)
[![coverage report](https://gitlab.com/talvbansal/laravel-public-sftp/badges/master/coverage.svg)](https://gitlab.com/talvbansal/laravel-public-sftp/commits/master)
[![License](https://poser.pugx.org/talvbansal/laravel-public-sftp/license)](https://gitlab.com/talvbansal/laravel-public-sftp/blob/master/licence)

## # Introduction
The default [sftp flysystem adapter](http://flysystem.thephpleague.com/adapter/sftp/) does not have a method to get the URL of files on the disk, instead a if you try and call the url() method on the disk instance a `RuntimeException` will be thrown.

I wrote this extension to the sftp adapter so that i could upload files using my [media-manager package](https://github.com/talvbansal/media-manager) and store the files on a remote server yet still have them in a publicly accessible place.

## # Installation

To get started, install public sftp adapter via the Composer package manager:

```bash
composer require talvbansal/laravel-public-sftp
```

Register the adapter in the `providers` array of your `config/app.php` configuration file:

```php
\TalvBansal\SftpPublic\Providers\SftpPublicServiceProvider::class,
```

After this a new disk will be automatically registered called `public-sftp`.

## # Configuration

Add the relevant configuration from the keys below to access your ssh enabled server:
```bash
SFTP_HOST=myserver.com
SFTP_USERNAME=username
SFTP_PW=password
SFTP_PRIVATE_KEY=/optional/path/to/private/key
SFTP_ROOT=/path/to/storage/folder
SFTP_PUBLIC_ROOT=https://public-url/to-folder-above
```

The new value to pay attention to here is `SFTP_PUBLIC_ROOT` which should be a publicly accessible URL that is serving the contents of the `SFTP_ROOT` folder.

So if you were using nginx or apache to serve the `/var/www/public` folder on a server over https on a domain of `example.com` the values would look like this:
```bash
SFTP_ROOT=/var/www/public
SFTP_PUBLIC_ROOT=https://example.com
```

You can now use the `public-sftp` disk as a [filesystem](https://laravel.com/docs/5.4/filesystem) within laravel and will be able to call the `url()` method on the disk instance to get a publicly accessible URL of an uploaded file.

## # Credits

- [Talv Bansal](https://gitlab.com/talvbansal)

# ## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.